package ictgradschool.industry.bounce;

import java.awt.*;

public class GemShape extends Shape
    {
    public GemShape(int x, int y, int deltaX, int deltaY)
        {
        super(x, y, deltaX, deltaY);
        }

    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height)
        {
        super(x, y, deltaX, deltaY, width, height);
        }

    @Override
    public void paint(Painter painter)
        {
            Polygon points = new Polygon();
        if (fWidth <= 40)
        {
            points.addPoint(fX + fWidth/2, fY);
            points.addPoint(fX+fWidth,fY+fHeight/2 );
            points.addPoint(fX+fWidth/2, fY+fHeight);
            points.addPoint(fX, fY+fHeight/2);
            painter.drawPolygon(points);
        }
        else{
            points.addPoint(fX+20, fY);
            points.addPoint(fX+fWidth-20, fY);
            points.addPoint(fX+fWidth, fY+fHeight/2);
            points.addPoint(fX+fWidth-20, fY+fHeight);
            points.addPoint(fX+20,fY+fHeight );
            points.addPoint(fX,fY+fHeight/2 );
            painter.drawPolygon(points);
        }
        }
    }

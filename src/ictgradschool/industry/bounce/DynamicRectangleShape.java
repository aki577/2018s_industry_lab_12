package ictgradschool.industry.bounce;

import java.awt.*;

import static java.awt.Color.black;

public class DynamicRectangleShape extends Shape
    {
    private boolean shouldBeFilled = false;
    private static Color DEFAULT_COLOR = black;
    private Color fcolor;

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY)
        {
        super(x, y, deltaX, deltaY);
        fcolor = DEFAULT_COLOR;
        }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height)
        {
        super(x, y, deltaX, deltaY, width, height);
        fcolor = DEFAULT_COLOR;
        }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Color color)
        {
        super(x,y,deltaX,deltaY,width,height);
        fcolor = color;
        }

    @Override
    public void move(int width, int height) {
        super.move(width, height);
        boolean history = shouldBeFilled;
        if (fX == 0 || fX == width - fWidth) {
            shouldBeFilled = true;
        }
        if (fY == 0 || fY == height - fHeight) {
            shouldBeFilled = false;
        }
        if((fX == 0 || fX == width - fWidth)&&(fY == 0 || fY == height - fHeight))
        {
            if(history){
                shouldBeFilled = false;
            }
            else{
                shouldBeFilled = true;
            }
        }
        }

    @Override
    public void paint(Painter painter)
        {
        if (shouldBeFilled) {
            // Draw fill rect
            painter.setColor(fcolor);
            painter.fillRect(fX, fY, fWidth, fHeight);
        }
        else{
            painter.drawRect(fX, fY, fWidth, fHeight);
        }
        // draw normal
        }
    }

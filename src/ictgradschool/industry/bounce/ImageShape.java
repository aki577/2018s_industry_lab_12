package ictgradschool.industry.bounce;

import java.io.File;

public class ImageShape extends Shape
    {
        private File image;

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height,File image)
        {
        super(x, y, deltaX, deltaY, width, height);
        this.image = image;
        }

    @Override
    public void paint(Painter painter)
        {
        painter.drawImg(image, fX, fY, fWidth, fHeight);
        }
    }
